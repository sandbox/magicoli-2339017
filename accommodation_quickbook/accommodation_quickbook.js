$(function() {
	$( "#adults" ).spinner({
		min: 1,
		max: 40});
		$( "#children" ).spinner({
			min: 1,
			max: 40});
			$( "#babies" ).spinner({
				min: 1,
				max: 40});

				$.datepicker.setDefaults( $.datepicker.regional[ "fr" ] );

				$.datepicker.setDefaults({
					firstDay: 1,
					autoSize: true,
					dateFormat: "dd/mm/yy",
					numberOfMonths: 2,
				});

				$( "#from" ).datepicker( {
					minDate: "+0",
					maxDate: "+1y",
					defaultDate: "+1d",
					onClose: function( selectedDate ) {
						$( "#to" ).datepicker( "option", "minDate", selectedDate );
					}
				});

				$( "#to" ).datepicker({
					autoSize: true,
					defaultDate: "+1w",
					maxDate: "+1y",
					onClose: function( selectedDate ) {
						$( "#from" ).datepicker( "option", "maxDate", selectedDate );
					}
				});
			});
